#include <stdio.h>
#include <strings.h>
#include <assert.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>

void					ft_bzero(void *ptr, size_t n);

static void test00_ft_bzero(void)
{
	char	control[10];
	char	subject[] = "123456789";

	bzero(control, 10);
	ft_bzero(subject, 10);
	assert(memcmp(control, subject, 10) == 0);
	subject[0] = 'a';
	ft_bzero(subject, 0);
	assert(subject[0] == 'a');
}

static void test01_ft_bzero(void)
{
	char	control[] = "123456789";
	char	subject[] = "123456789";

	bzero(control, 3);
	ft_bzero(subject, 3);
	assert(memcmp(subject, control, 10) == 0);
}

char 					* ft_strcat(char *str1, char const * str2);

static void test00_ft_strcat(void)
{
	char	buf[9];

	bzero(buf, 9);
	ft_strcat(buf, "");
	ft_strcat(buf, "Bon");
	ft_strcat(buf, "j");
	ft_strcat(buf, "our.");
	printf("%s\n", buf);
	ft_strcat(buf, "");
	assert(strcmp(buf, "Bonjour.") == 0);
	assert(buf == ft_strcat(buf, ""));
}

int 					ft_isalpha(int c);

static void test00_ft_isalpha(void)
{
	assert(ft_isalpha('a') == isalpha('a'));
	assert(ft_isalpha('a' + 0x100) == isalpha('a' + 0x100));
	assert(ft_isalpha('2') == isalpha('2'));
	assert(ft_isalpha('Z') == isalpha('Z'));
	assert(ft_isalpha('t') == isalpha('t'));
	assert(ft_isalpha(0) == isalpha(0));
	assert(ft_isalpha(1) == isalpha(1));
	assert(ft_isalpha(9999) == isalpha(9999));
	assert(ft_isalpha('1') == isalpha('1'));
	assert(ft_isalpha('2') == isalpha('2'));
	assert(ft_isalpha('A') == isalpha('A'));
	assert(ft_isalpha('Z') == isalpha('Z'));
	assert(ft_isalpha(' ') == isalpha(' '));
	assert(ft_isalpha('%') == isalpha('%'));
	assert(ft_isalpha('\t') == isalpha('\t'));
	assert(ft_isalpha('\n') == isalpha('\n'));
	assert(ft_isalpha('\v') == isalpha('\v'));
	assert(ft_isalpha('\b') == isalpha('\b'));
	assert(ft_isalpha(7) == isalpha(7));
}

int 					ft_isdigit(int c);

static void test00_ft_isdigit(void)
{
	assert(ft_isdigit('a') == isdigit('a'));
	assert(ft_isdigit('a' + 0x100) == isdigit('a' + 0x100));
	assert(ft_isdigit('2') == isdigit('2'));
	assert(ft_isdigit('Z') == isdigit('Z'));
	assert(ft_isdigit('t') == isdigit('t'));
	assert(ft_isdigit(0) == isdigit(0));
	assert(ft_isdigit(1) == isdigit(1));
	assert(ft_isdigit(9999) == isdigit(9999));
	assert(ft_isdigit('1') == isdigit('1'));
	assert(ft_isdigit('2') == isdigit('2'));
	assert(ft_isdigit('A') == isdigit('A'));
	assert(ft_isdigit('Z') == isdigit('Z'));
	assert(ft_isdigit(' ') == isdigit(' '));
	assert(ft_isdigit('%') == isdigit('%'));
	assert(ft_isdigit('\t') == isdigit('\t'));
	assert(ft_isdigit('\n') == isdigit('\n'));
	assert(ft_isdigit('\v') == isdigit('\v'));
	assert(ft_isdigit('\b') == isdigit('\b'));
	assert(ft_isdigit(7) == isdigit(7));
}

int 					ft_isalnum(int c);

static void test00_ft_isalnum(void)
{
	assert(ft_isalnum('a') == isalnum('a'));
	assert(ft_isalnum('a' + 0x100) == isalnum('a' + 0x100));
	assert(ft_isalnum('2') == isalnum('2'));
	assert(ft_isalnum('Z') == isalnum('Z'));
	assert(ft_isalnum('t') == isalnum('t'));
	assert(ft_isalnum(0) == isalnum(0));
	assert(ft_isalnum(1) == isalnum(1));
	assert(ft_isalnum(9999) == isalnum(9999));
	assert(ft_isalnum('1') == isalnum('1'));
	assert(ft_isalnum('2') == isalnum('2'));
	assert(ft_isalnum('A') == isalnum('A'));
	assert(ft_isalnum('Z') == isalnum('Z'));
	assert(ft_isalnum(' ') == isalnum(' '));
	assert(ft_isalnum('%') == isalnum('%'));
	assert(ft_isalnum('\t') == isalnum('\t'));
	assert(ft_isalnum('\n') == isalnum('\n'));
	assert(ft_isalnum('\v') == isalnum('\v'));
	assert(ft_isalnum('\b') == isalnum('\b'));
	assert(ft_isalnum(7) == isalnum(7));
}

int 					ft_isascii(int c);

static void test00_ft_isascii(void)
{
	assert(ft_isascii('a') == isascii('a'));
	assert(ft_isascii('a' + 0x100) == isascii('a' + 0x100));
	assert(ft_isascii('2') == isascii('2'));
	assert(ft_isascii('Z') == isascii('Z'));
	assert(ft_isascii('t') == isascii('t'));
	assert(ft_isascii(0) == isascii(0));
	assert(ft_isascii(1) == isascii(1));
	assert(ft_isascii(9999) == isascii(9999));
	assert(ft_isascii('1') == isascii('1'));
	assert(ft_isascii('2') == isascii('2'));
	assert(ft_isascii('A') == isascii('A'));
	assert(ft_isascii('Z') == isascii('Z'));
	assert(ft_isascii(' ') == isascii(' '));
	assert(ft_isascii('%') == isascii('%'));
	assert(ft_isascii('\t') == isascii('\t'));
	assert(ft_isascii('\n') == isascii('\n'));
	assert(ft_isascii('\v') == isascii('\v'));
	assert(ft_isascii('\b') == isascii('\b'));
	assert(ft_isascii(7) == isascii(7));
}

int 					ft_isprint(int c);

static void test00_ft_isprint(void)
{
	assert(ft_isprint('a') == isprint('a'));
	assert(ft_isprint('a' + 0x100) == isprint('a' + 0x100));
	assert(ft_isprint('2') == isprint('2'));
	assert(ft_isprint('Z') == isprint('Z'));
	assert(ft_isprint('t') == isprint('t'));
	assert(ft_isprint(0) == isprint(0));
	assert(ft_isprint(1) == isprint(1));
	assert(ft_isprint(9999) == isprint(9999));
	assert(ft_isprint('1') == isprint('1'));
	assert(ft_isprint('2') == isprint('2'));
	assert(ft_isprint('A') == isprint('A'));
	assert(ft_isprint('Z') == isprint('Z'));
	assert(ft_isprint(' ') == isprint(' '));
	assert(ft_isprint('%') == isprint('%'));
	assert(ft_isprint('\t') == isprint('\t'));
	assert(ft_isprint('\n') == isprint('\n'));
	assert(ft_isprint('\v') == isprint('\v'));
	assert(ft_isprint('\b') == isprint('\b'));
	assert(ft_isprint(7) == isprint(7));
}

int 					ft_toupper(int c);

static void test00_ft_toupper(void)
{
	assert(ft_toupper('a') == toupper('a'));
	assert(ft_toupper('a' + 0x100) == toupper('a' + 0x100));
	assert(ft_toupper('2') == toupper('2'));
	assert(ft_toupper('Z') == toupper('Z'));
	assert(ft_toupper('t') == toupper('t'));
	assert(ft_toupper(0) == toupper(0));
	assert(ft_toupper(1) == toupper(1));
	assert(ft_toupper(9999) == toupper(9999));
	assert(ft_toupper('1') == toupper('1'));
	assert(ft_toupper('2') == toupper('2'));
	assert(ft_toupper('A') == toupper('A'));
	assert(ft_toupper('Z') == toupper('Z'));
	assert(ft_toupper(' ') == toupper(' '));
	assert(ft_toupper('%') == toupper('%'));
	assert(ft_toupper('\t') == toupper('\t'));
	assert(ft_toupper('\n') == toupper('\n'));
	assert(ft_toupper('\v') == toupper('\v'));
	assert(ft_toupper('\b') == toupper('\b'));
	assert(ft_toupper(7) == toupper(7));
}

int 					ft_tolower(int c);

static void test00_ft_tolower(void)
{
	assert(ft_tolower('a') == tolower('a'));
	assert(ft_tolower('a' + 0x100) == tolower('a' + 0x100));
	assert(ft_tolower('2') == tolower('2'));
	assert(ft_tolower('Z') == tolower('Z'));
	assert(ft_tolower('t') == tolower('t'));
	assert(ft_tolower(0) == tolower(0));
	assert(ft_tolower(1) == tolower(1));
	assert(ft_tolower(9999) == tolower(9999));
	assert(ft_tolower('1') == tolower('1'));
	assert(ft_tolower('2') == tolower('2'));
	assert(ft_tolower('A') == tolower('A'));
	assert(ft_tolower('Z') == tolower('Z'));
	assert(ft_tolower(' ') == tolower(' '));
	assert(ft_tolower('%') == tolower('%'));
	assert(ft_tolower('\t') == tolower('\t'));
	assert(ft_tolower('\n') == tolower('\n'));
	assert(ft_tolower('\v') == tolower('\v'));
	assert(ft_tolower('\b') == tolower('\b'));
	assert(ft_tolower(7) == tolower(7));
}

int 					ft_puts(char const * str);

static void test00_ft_puts(void)
{
	ft_puts("CHATALORS");
}

unsigned int 					ft_strlen(char * str);

static void test00_ft_strlen(void)
{
	assert(ft_strlen("chat") == strlen("chat"));
	assert(ft_strlen("") == strlen(""));
	assert(ft_strlen("aaa\0aaa") == strlen("aaa\0aaa"));
}

static void test01_ft_strlen(void)
{
	int test_len = 1000 * 1000;
	char str[test_len];

	memset(str, 'a', test_len);
	str[test_len - 1] = '\0';
	assert(ft_strlen(str) == strlen(str));
}

void * ft_memset(void *b, int c, size_t len);

static void test00_ft_memset(void)
{
	char	b1[100], b2[100];

	ft_memset(b1, 42, 100);
	memset(b2, 42, 100);
	assert(memset(b1, 99, 0) == ft_memset(b1, 99, 0));
	assert(memcmp(b1, b2, 100) == 0);
	b1[0] = 1;
	ft_memset(b1, 0, 0);
	assert(b1[0] == 1);
}

static void test01_ft_memset(void)
{
	assert(memcmp(memset(strdup("abcd"), 'A', 5), ft_memset(strdup("abcd"), 'A', 5), 5) == 0);
}

static void test02_ft_memset(void)
{	
	assert(memcmp(memset(strdup("abcd"), 0, 0), ft_memset(strdup("abcd"), 0, 0), 5) == 0);
}

void * ft_memcpy(void * dst, const void * src, size_t n);

static void test00_ft_memcpy(void)
{
	char	b1[100], b2[100];

	memset(b1, 33, 100);
	memset(b2, 63, 100);

	ft_memcpy(b1, b2, 100);
	assert(memcmp(b1, b2, 100) == 0);
	assert(ft_memcpy(b1, b2, 0) == b1);
}

char * ft_strdup(const char *s1);

static void test00_ft_strdup(void)
{
	assert(strcmp(ft_strdup("aaaaa"), "aaaaa") == 0);
}

static void test01_ft_strdup(void)
{
	assert(strcmp(ft_strdup(""), "") == 0);
}

static void test02_ft_strdup(void)
{
	char	*c;

	c = "AbC";
	assert(c != ft_strdup(c));
}

void					ft_cat(int fd);

static void test00_ft_cat(void)
{
	int fd = open("Makefile", O_RDONLY);
	ft_cat(fd);
	close(fd);
}

static void test01_ft_cat(void)
{
	ft_cat(-1);
}

static void test02_ft_cat(void)
{
	ft_cat(0);
}

int						main(void) {
	printf("\n\033[31mPART1\033[0m\n");
	printf("\n\033[35mTest00 ft_bzero\033[0m\n");
	test00_ft_bzero();
	printf("\033[36mEnd test fr_bzero\033[0m\n");

	printf("\n\033[35mTest01 ft_bzero\033[0m\n");
	test01_ft_bzero();
	printf("\033[36mEnd test fr_bzero\033[0m\n");

	printf("\n\033[35mTest00 ft_strcat\033[0m\n");
	test00_ft_strcat();
	printf("\033[36mEnd test ft_strcat\033[0m\n");

	printf("\n\033[35mTest00 ft_isalpha\033[0m\n");
	test00_ft_isalpha();
	printf("\033[36mEnd test ft_isalpha\033[0m\n");

	printf("\n\033[35mTest00 ft_isdigit\033[0m\n");
	test00_ft_isdigit();
	printf("\033[36mEnd test ft_isdigit\033[0m\n");

	printf("\n\033[35mTest00 ft_isalnum\033[0m\n");
	test00_ft_isalnum();
	printf("\033[36mEnd test ft_isalnum\033[0m\n");

	printf("\n\033[35mTest00 ft_isascii\033[0m\n");
	test00_ft_isascii();
	printf("\033[36mEnd test ft_isascii\033[0m\n");

	printf("\n\033[35mTest00 ft_isprint\033[0m\n");
	test00_ft_isprint();
	printf("\033[36mEnd test ft_isprint\033[0m\n");

	printf("\n\033[35mTest00 ft_toupper\033[0m\n");
	test00_ft_toupper();
	printf("\033[36mEnd test ft_toupper\033[0m\n");

	printf("\n\033[35mTest00 ft_tolower\033[0m\n");
	test00_ft_tolower();
	printf("\033[36mEnd test ft_tolower\033[0m\n");

	printf("\n\033[35mTest00 ft_puts\033[0m\n");
	test00_ft_puts();
	printf("\033[36mEnd test ft_puts\033[0m\n");

	printf("\n\033[31mPART2\033[0m\n");
	printf("\n\033[35mTest00 ft_strlen\033[0m\n");
	test00_ft_strlen();
	printf("\033[36mEnd test ft_strlen\033[0m\n");

	printf("\n\033[35mTest01 ft_strlen\033[0m\n");
	test01_ft_strlen();
	printf("\033[36mEnd test ft_strlen\033[0m\n");

	printf("\n\033[35mTest00 ft_memset\033[0m\n");
	test00_ft_memset();
	printf("\033[36mEnd test ft_memset\033[0m\n");

	printf("\n\033[35mTest01 ft_memset\033[0m\n");
	test01_ft_memset();
	printf("\033[36mEnd test ft_memset\033[0m\n");

	printf("\n\033[35mTest02 ft_memset\033[0m\n");
	test02_ft_memset();
	printf("\033[36mEnd test ft_memset\033[0m\n");

	printf("\n\033[35mTest00 ft_memcpy\033[0m\n");
	test00_ft_memcpy();
	printf("\033[36mEnd test ft_memcpy\033[0m\n");

	printf("\n\033[35mTest00 ft_strdup\033[0m\n");
	test00_ft_strdup();
	printf("\033[36mEnd test ft_strdup\033[0m\n");

	printf("\n\033[35mTest01 ft_strdup\033[0m\n");
	test01_ft_strdup();
	printf("\033[36mEnd test ft_strdup\033[0m\n");

	printf("\n\033[35mTest02 ft_strdup\033[0m\n");
	test02_ft_strdup();
	printf("\033[36mEnd test ft_strdup\033[0m\n");

	printf("\n\033[31mPART3\033[0m\n");
	printf("\n\033[35mTest00 ft_cat\033[0m\n");
	test00_ft_cat();
	printf("\033[36mEnd test fr_cat\033[0m\n");

	printf("\n\033[35mTest01 ft_cat\033[0m\n");
	test01_ft_cat();
	printf("\033[36mEnd test fr_cat\033[0m\n");

	printf("\n\033[35mTest02 ft_cat\033[0m\n");
	test02_ft_cat();
	printf("\033[36mEnd test fr_cat\033[0m\n");

	return 0;
}
