section .text
	global _ft_strcat
	extern _ft_strlen

_ft_strcat:
	push rdi
	sub rcx, rcx
	not rcx
	xor al, al
	cld
	repne	scasb
	dec rdi

	push rdi
	push rsi

	mov rdi, rsi
	call _ft_strlen
	mov rcx, rax
	
	pop rsi
	pop rdi

	cld
	repne	movsb

	pop rax
	ret
