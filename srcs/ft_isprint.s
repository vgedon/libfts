section .data
	cmp1: equ 32
	cmp2: equ 126

section .text
		global _ft_isprint

_ft_isprint:
	cmp rdi, cmp1
	jb false
	cmp rdi, cmp2
	ja false
	jmp true

true:
	mov rax, 1
	jmp return

false:
	mov rax, 0
	jmp return

return:
	ret