section .text
	global _ft_memset
	extern _ft_strlen

_ft_memset:
	push rdi

	mov rcx, rdx
	mov rax, rsi
	
	cld
	repe	stosb

	pop rax
	ret
