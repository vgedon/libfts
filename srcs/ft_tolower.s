section .data
	cmp1: equ 'A'
	cmp2: equ 'Z'
	lower: equ 32

section .text
	global _ft_tolower

_ft_tolower:
	cmp rdi, cmp1
	jb return
	cmp rdi, cmp2
	ja return
	add rdi, lower

return:
	mov rax, rdi
	ret
