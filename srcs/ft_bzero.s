section .text
	global _ft_bzero

_ft_bzero:
	mov rcx, rsi
	xor al, al
	
	cld
	repe	stosb

	ret