section .data
	cmp1: equ '0'
	cmp2: equ '9'

section .text
		global _ft_isdigit

_ft_isdigit:
	cmp rdi, cmp1
	jb false
	cmp rdi, cmp2
	ja false
	jmp true

true:
	mov rax, 1
	jmp return

false:
	mov rax, 0
	jmp return

return:
	ret