section .data
	cmp1: equ 'a'
	cmp2: equ 'z'
	upper: equ 32

section .text
	global _ft_toupper

_ft_toupper:
	cmp rdi, cmp1
	jb return
	cmp rdi, cmp2
	ja return
	sub rdi, upper

return:
	mov rax, rdi
	ret
