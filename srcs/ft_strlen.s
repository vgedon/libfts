section .text
	global _ft_strlen

_ft_strlen:
	xor rcx, rcx
	not rcx
	xor rax, rax
	cld
	repnz	scasb
	not rcx
	dec rcx
	mov rax, rcx
	ret