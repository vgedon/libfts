section .data
	cmp1: equ '0'
	cmp2: equ '9'
	cmp3: equ 'A'
	cmp4: equ 'Z'
	cmp5: equ 'a'
	cmp6: equ 'z'

section .text
		global _ft_isalnum

_ft_isalnum:
	cmp rdi, cmp1
	jb false
	cmp rdi, cmp6
	ja false
	cmp rdi, cmp2
	jbe true
	cmp rdi, cmp5
	jae true
	cmp rdi, cmp3
	jb false
	cmp rdi, cmp4
	ja false
	jmp true

true:
	mov rax, 1
	jmp return

false:
	mov rax, 0
	jmp return

return:
	ret