section .data
	cmp1: equ 'A'
	cmp2: equ 'Z'
	cmp3: equ 'a'
	cmp4: equ 'z'

section .text
		global _ft_isalpha

_ft_isalpha:
	cmp rdi, cmp1
	jb false
	cmp rdi, cmp4
	ja false
	cmp rdi, cmp2
	jbe true
	cmp rdi, cmp3
	jae true
	jmp false

true:
	mov rax, 1
	jmp return

false:
	mov rax, 0
	jmp return

return:
	ret