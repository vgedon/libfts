section .text
	global _ft_strdup
	extern _ft_strlen
	extern _malloc

_ft_strdup:
	push rdi

	call _ft_strlen
	mov rcx, rax

	mov rdi, rcx
	push rcx
	push rdi
	call _malloc
	pop rdi
	mov rdi, rax

	pop rcx
	pop rsi
	cld
	repne	movsb
	ret