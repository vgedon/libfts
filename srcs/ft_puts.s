section .data
	endl: db 10

section .text
	global _ft_puts
	extern _ft_strlen

_ft_puts:
	push rdi			;save rdi for strlen
	call _ft_strlen
	mov rdx, rax
	mov rax, 0x2000004
	pop rdi				;reload rdi
	mov rsi, rdi
	mov rdi, 1
	syscall

	mov rax, 0x2000004
	mov rdx, 1
	mov rsi, endl
	mov rdi, 1
	syscall

	ret