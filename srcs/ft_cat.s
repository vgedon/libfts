section .data
	string: times 256 db 0
	endl: db 10

section .text
	global _ft_cat

_ft_cat:
	mov rbx, rdi

_loop1:
	mov rax, 0x2000003
	mov rdi, rbx
	mov rsi, string
	mov rdx, 255
	syscall
	jc end
	cmp rax, 0
	jle end

	mov rdi, 1
	mov rdx, rax
	mov rax, 0x2000004
	syscall
	jc end
	jmp _loop1

end:
	sub rax, rax
	ret