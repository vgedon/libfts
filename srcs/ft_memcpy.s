section .text
	global _ft_memcpy
	extern _ft_strlen

_ft_memcpy:
	push rdi

	mov rcx, rdx
	xor rax, rax

	cld
	repne	movsb

	pop rax
	ret
