# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/11/19 15:43:56 by vgedon            #+#    #+#              #
#    Updated: 2014/05/15 11:47:36 by vgedon           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libfts.a

SRCDIR = srcs
SRC = 	ft_bzero.s\
		ft_strcat.s\
		ft_isalpha.s\
		ft_isdigit.s\
		ft_isalnum.s\
		ft_isascii.s\
		ft_isprint.s\
		ft_toupper.s\
		ft_tolower.s\
		ft_puts.s\
		ft_strlen.s\
		ft_memset.s\
		ft_memcpy.s\
		ft_strdup.s\
		ft_cat.s

OBJDIR = .obj
OBJ = $(addprefix $(OBJDIR)/, $(SRC:.s=.o))

ASM = nasm
ASMFLAGS = -f macho64

AR = ar
ARFLAGS = -rcs

RM = rm
RMFLAGS = -rf

INCLUDES = -I includes

GREEN = \033[32m
NORMAL = \033[0m

all: $(NAME)
	@echo -n

$(NAME): $(OBJDIR) $(OBJ)
	@printf "\n\033[1;4;36mCompiling $(NAME)\033[0m\n"
	@$(AR) $(ARFLAGS) $(NAME) $(OBJ)
	@printf "\033[36m%-50s\033[0m[$(GREEN) OK $(NORMAL)]\n\n" "Compiling $@"


$(OBJDIR):
	@/bin/mkdir $(OBJDIR);

$(addprefix $(OBJDIR)/, %.o): $(addprefix $(SRCDIR)/, %.s)
	@$(ASM) $(ASMFLAGS) $< -o $@


clean:
	@ $(RM) $(RMFLAGS) $(OBJDIR)

fclean: clean
	@ $(RM) $(RMFLAGS) $(NAME)

re: fclean all

exec: all
	@gcc -Wall -Werror -Wextra $(NAME) part1and2.c && ./a.out

.PHONY: re clean fclean
